<?php

namespace app\exam\controller;
use think\Controller;
use  app\exam\model\Paper as paperModel;
use think\Request;
use think\facade\Session;


class Paper extends Controller{


    //试卷列表
    public function paper(){
        
        return $this->fetch('paper');
    }

    public function index()
    {

        //获得数据总数
        $paperModel = new paperModel();
        $data = $paperModel->getpaper();
        
        $allcount = count($data);
        
        //获取传递的分页参数
        $page=request()->param('page');
        $limit=request()->param('limit');
        $start=$limit*($page-1);

        //分页查询
        $userpage = $paperModel->getpage($start,$limit);
        $res = [
                    'code'=>0,
                    'msg'=>'返回成功',
                    'count'=>$allcount,
                    'data'=>$userpage
                ];
        return json($res);
    }

    //试卷详情
    public function detailPaper(Request $request){
        $id = $request->GET('id');
        $paperModel = new paperModel();
        $data = $paperModel->getdetailp($id);
        $pname = $paperModel->getpname($id);
        $this->assign('pList',$data);
        $this->assign('pname',$pname);
        return $this->fetch('detailp');
    }

    //  添加试卷
    public function addp(Request $request){
        if($request->isAjax()){
            $pname = $request->param('pname');
            $subject = $request->param('subject');
            $paperModel = new paperModel();
            $res = $paperModel->addp($pname,$subject);
            return $res;
        }else{
            return false;
        }
    }
    //修改试卷
    public function updp(Request $request){
        if($request->isAjax()){
            $pname = $request->param('pname');
            $subject = $request->param('subject');
            $id = $request->param('id');
            $testq = $request->param('testq');
            $paperModel = new paperModel();
            $res = $paperModel->updp($pname,$subject,$id,$testq);
            return $res;
        }else{
            return false;
        }
    }

    //删除试卷
    public function deletep(Request $request){
        if($request->isAjax()){
            $id = $request->param('id');
            $paperModel = new paperModel();
            $res = $paperModel->deleltp($id);
            return $res;
        }else{
            return false;
        }
    }


    //用户可做试卷
    public function spaper(Request $request){
        $id = $request->GET('id');
        $subject = $request->GET('subject');
        $paperModel = new paperModel();
        $data = $paperModel->getdetailp($id);
        $pname = $paperModel->getpname($id);
        $this->assign('pList',$data);
        $this->assign('pname',$pname);
        $this->assign('subject',$subject);
        return $this->fetch('spaper');
    }
    //提交试卷
    public function dospaper(Request $request){
       
        $pname =$request->param('pname');
        $userid = $request->param('userid');
        $ctime = $request->param('ctime');
        $subject = $request->param('subject');
        $answer = [];
        for($i=1;$i<=25;$i++){
            $k = $request->param('answer'.$i);
            array_push($answer,$k[0]);
        }
        $ans = [];
        $answ = [];//值是答案数组
        foreach($answer as $item=>$val){
            $a = explode('|',$val);
         array_push($ans,$a[0]);
         array_push($answ,$a[1]);

        }
        $answe = [];//键是题号，值是答案
        $answe = array_combine( $ans, $answ );
       $paperModel = new paperModel();
       $res = $paperModel->spaper($pname,$subject,$userid,$ctime,$answe,$ans);
       if($res>0){
           $this->success('提交成功','exam/index/content');
       }else{
           $this->error("提交失败");
       }


    }

    //用户的做过的试卷
    public function upaper(){
        return $this->fetch('upaper');
    }


    //试卷记录赋值
    public function upaperlist(){
        //获得数据总数
        $userdata = Session::get('userdata');
        $id = $userdata['id'];
        $paperModel = new paperModel();
        $data = $paperModel->getspaper($id); 
        $allcount = count($data);
        //获取传递的分页参数
        $page=request()->param('page');
        $limit=request()->param('limit');
        $start=$limit*($page-1);
        //分页查询
        $userpage = $paperModel->getspage($start,$limit,$id);
        $res = [
                    'code'=>0,
                    'msg'=>'返回成功',
                    'count'=>$allcount,
                    'data'=>$userpage
                ];
        return json($res);

    }
    //考生试卷详情

    public function detailsp(Request $request){
        $id = $request->param('id');//id为spaper的id，为了取出答案
        $paperModel = new paperModel();
        $upaper = $paperModel->getupaper($id); 
        $this->assign('upaper',$upaper);
        
        return $this->fetch('detailsp');
    }

    //历史试卷管理
    public function adspaperlist(){
        return $this->fetch('adpaperlist');
    }

    //历史试卷列表赋值
    public function adspaperval(){
        $paperModel = new paperModel();
        $data = $paperModel->getadspaper(); 
        $allcount = count($data);
        //获取传递的分页参数
        $page=request()->param('page');
        $limit=request()->param('limit');
        $start=$limit*($page-1);
        //分页查询
        $userpage = $paperModel->getadspage($start,$limit);
        $res = [
                    'code'=>0,
                    'msg'=>'返回成功',
                    'count'=>$allcount,
                    'data'=>$userpage
                ];
        return json($res);
    }

    //删除历史试卷记录
    public function deleteadsp(Request $request){
        if($request->isAjax()){
            $id = $request->param('id');
            $paperModel = new paperModel();
            $res = $paperModel->deleltadsp($id);
            return $res;
        }else{
            return false;
        }
    }



























    //删除历史试卷记录
    public function deletesp(Request $request){
        $id = $request->param('id');
        if($request->isAjax()){
            $paperModel = new paperModel();
            $res = $paperModel->deletesp($id);
            return $res; 

        }else{
            return false;
        }

    }





}