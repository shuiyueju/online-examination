<?php

namespace app\exam\controller;
use think\Controller;
use think\Request;
use app\exam\model\User;

use think\facade\Session;

class Login extends Controller{


    //管理员登录
    public function adminLogin(){
        return $this->fetch('adminLogin');
    }
    //用户登录
    public function homeLogin(){
        return $this->fetch('homeLogin');
    }

     //做注册数据处理
     public function Register(Request $request){
        // config::set("default_return_type","json");
        if($request->isAjax()){
          
            $udata =  $request->post();
            // dump($udata);
            $name = $udata['name'];
            $password = $udata['password'];
            $email = $udata['email'];
            $userModel = new User();
            $res = $userModel->register($name,$password,$email);
            return $res;


        }else{
            $this->error('非ajax请求！');
        }


    }

    public function login(Request $request){
        if($request->isAjax()){
            $user = $request->post();
            $email = $user['email'];
            $password = $user['password'];
            $userModel = new User();
            $data = $userModel->login($email,$password); 
            if($data){
                Session::set('userdata',$data);
                return $data;
            }else{
                return $data;
            }
          
        }else{
            $this->error('非ajax请求！');
        }
    }
    //修改密码
    public function editpwd(Request $request){
        if($request->isAjax()){
            $password = $request->post('password');
            $userModel = new User();
            $res = $userModel->editpwd($password);
            return $res;
        }else{
            $this->error('非ajax请求！');
        }
    }

    //退出登录
    public function out(){
        Session::clear();
        return true;
  
    }












}