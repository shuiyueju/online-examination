<?php

namespace app\exam\controller;
use think\Controller;

class Index extends Controller{


    //首页
    public function index(){
        return $this->fetch('index');
    }

    //首页内容
    public function content(){
        return $this->fetch('content');
    }




}