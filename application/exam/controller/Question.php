<?php

namespace app\exam\controller;
use think\Controller;
use app\exam\model\Question as questionModel;
use think\Request;

class Question extends Controller{



    public function question(){ 


        return $this->fetch('question');

    }

    public function index()
    {

        //获得数据总数
        $questionModel = new questionModel();
        $data = $questionModel->getQuestion();
        
        $allcount = count($data);
        
        //获取传递的分页参数
        $page=request()->param('page');
        $limit=request()->param('limit');
        $start=$limit*($page-1);

        //分页查询
        $userpage = $questionModel->getpage($start,$limit);
        $res = [
                    'code'=>0,
                    'msg'=>'返回成功',
                    'count'=>$allcount,
                    'data'=>$userpage
                ];
        return json($res);
    }


    //删除试题
    public function deleteQ(Request $request){
        if($request->isAjax()){
            $deleteID = $request->param('id');
            $questionModel = new questionModel();
            $res = $questionModel->deleteQ($deleteID);
            return $res;
        }else{
            return false;
        }
    }


    //增加试题
    public function addQ(Request $request){
        if($request->isAjax()){
            $addData = $request->param();
            $questionModel = new questionModel();
            $res = $questionModel->addQ($addData);
            return $res;
        }else{
            return false;
        }
    }
    //修改试题
    public function updateQ(Request $request){
        if($request->isAjax()){
            $updateData = $request->param();
            $questionModel = new questionModel();
            $res = $questionModel->updateQ($updateData);
            return $res;
        }else{
            return false;
        }
    }










}