<?php


namespace app\exam\model;

use think\Model;

class Question extends Model{

    public function getQuestion(){
        return $this->select();
    }

    public function getpage($start,$limit){
        return $this->order('id','asc')->limit($start,$limit)->select();
    }

    public function deleteQ($id){
        return $this->where('id',$id)->delete();
    }

    public function addQ($addData){
        $questionList = [
            "content" => $addData["content"],
            "subject"=>$addData["subject"],
            "type"=>$addData["type"],
            "sa" => $addData["sa"],
            "sb"=>$addData["sb"],
            "sc" => $addData["sc"],
            "sd"=>$addData["sd"],
            "answer" => $addData["answer"],


        ];

        return $this->insert($questionList);

    }


    public function updateQ($updateData){
        $id = $updateData["id"];
        $updateList=[
            'content'=>$updateData['content'],
            'subject'=>$updateData['subject'],
            'sa'=>$updateData['sa'],
            'sb'=>$updateData['sb'],
            'sc'=>$updateData['sc'],
            'sd'=>$updateData['sd'],
            'answer'=>$updateData['answer'],
 

        ];

        return $this->where('id',$id)->update($updateList);
    }
}