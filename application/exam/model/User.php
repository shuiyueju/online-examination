<?php


namespace app\exam\model;

use think\facade\Session;
use think\Model;

class User extends Model{

    public function register($name,$password,$email){
        $num = $this->where('email',$email)->select();
        $count = $num->count();
        if($count != 0 ){
            return 2;
        }else{
            $list = [
                'name'=>$name,
                'password'=>$password,
                'email'=>$email,
                'type'=>0
                
            ];
            
           $data =  $this->insert($list);
           return $data;

        }      
        
    }

    public function login($email,$password){
        $data = $this->where('email',$email)->find();
        if($data){
            if($data['password'] == $password){
                return $data;
            }else{
                return false;
            }
        }else{
            return false;
        }
    }


    public function editpwd($password){
        $userdata = Session::get('userdata');
        $map["id"] = ["IN",$userdata['id']];
        return $this->where($map)->update(["password"=>md5($password)]);
    }

    public function getUser(){
        return $this->select();
    }

    public function getpage($start,$limit){
        return $this->order('id','asc')->limit($start,$limit)->select();
    }

    public function deluser($id){
        return $this->where('id',$id)->delete();
    }

    
}